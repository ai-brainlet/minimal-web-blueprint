# Basic Boi Blog

I like the idea of the web being a platform for content. Making things for the web is way too hard these days, and for most people the only
reasonable alternative is to use middle-ware for getting set up on the web (Wordpress, Squarespace, Gatsby, Hugo...). This repo is a small step
towards building a new normal for how web content publishing can look when our primary concern is delivering content for an array of different 
browsers and devices of varying capabilities. 

Content Editors should be able to quickly use basic and simple tools to generate new web pages which adapts to the content, not the other way 
around. The main issue I have with the current generation of tools for this end is that they are very large and yet not dynamic enough to truly 
utilize the web as the native platform for diverse content that it is was always poised to be. 

This bloated, oppressive way of building the web locks you into themes and makes all of your content look generic, despite still not usually 
getting the basics of responsiviity and progressive enhancement right.

Soupault is great software, and more web developers, especially ofc the indies, should check it out. 

Recipe:

  - Soupault : rather than an static site generator, Soupault is a web pre-compiler written by 
  - Awsm CSS : no page theme/content-typing system like Hugo and the boys, this blueprint uses awsmcss
  - Static Mastodon Feed : small js script developed by @idotj
  - Pandoc : Pandoc is my bestfriend. soupault lets you be bestfriends with whichever md converter you want

here's how it works:

# Soupault

In simple terms, pieces of html are programatically glued together and or injected with markdown at build time.

'Components' are just small html files with their own encapsulated css and js just like old school `<style>` and `<script>` tags

This is meant for markdown based, mostly static sites, similar to Hugo or Jekyll, although the DX makes this very powerful.

Soupault can inject lots of output from external unix programs, for static rendering of things like code highlighting and math formulas

It can also work with meta data in the markdown, so you can automate much of the updating process of your website, even while building 
completely unique pages. Only Souault can do this, since other SSG's lock you into a 'theme'.

Soupault is very minimal and hackable, as its ultimately just a bunch of build scripts glued together wth a lua config API. 
It does not include a dev server or a markdown parser. This means you can use whatever variant of markdown you want, 
as long as it outputs html it works ! 

## WTF Are Widgets

## Homepage (index.html)

## Static Components (Templates)

## Included Static .html Files

## AWSM CSS, Custom Page Layouts, grids

ofc, your page will need styles. The go to solution a growing number of text based web pages is a classless
css library. This is .css file which basically provides default stylings for all the native html elements.

I use awsm.css for this, as it has features which do some nice formatting of inlined elements. Think of it as an extension of 
a traditional reset or normalize css file, except with much more defualt formating to make the document look great and responsive with
no (barely) any css needed. 

* I say barely simply because you will likely want to configure the look of your site. Now we can do it at the level of typography, color,
and general aesthetic rather than genrating tons of CSS just to get the page to look somewhat presentable.

That being said, you can add as much custom css as you want. You can even inject separate .css libs to different static templates, or
style each one in it's own individual template file.

In my case awsm serves as the default base for basically all the pages, but they are extended with custom css. For instance, to add a grid
we can simply use Soupault to inject css grid file into the pages which need it. Or, we can write a grid for that template on the fly. 

When Soupault glues our page together, It will know based on our config how to build our semantic html pages, and so wherever we tell
Soupault to manipulate the DOM, it will. Otherwise it just injects the site with markdown content straight away, and awsm will 'format' it
the way we expect without needing to define any css classless.

To be sure, since our new css is just a inline style tag in the .html file at the end of the compilation process, our css will override
the default css brought in by the header.

## More CSS options

Because Soupault can inject markdown arbitrarily into templates, we can use static css component systems, such as Tachyons or Foundations.
This could be useful if we don't want to include any css libs at all !

In this case everything would be individual, inline css encapsualted to each template file just like in this blueprint, but without the 
simplicity of not having to write new css classes.

We can do the same thing with any vanilla css and js we find in the wild. Just throw the markup in a file, and call it somewhere via a widget
My convention is to throw all the css and js related to a piece of markup we may want to re use, just like a component in a framework.
That said, you can also just use Soupault to inject the right css link tag per widget (single file coponent) so the related scripts and 
styles are not duplicated in your build

My opinion is to keep these 'component' scripts small, but ultimately not worry about inline code to much. Afterall, each html file is only
parsed when it's opened, so including libraries and scripts over and over again seems like the more bloated option.
 
But that said, Soupault enforces nothing so you can build the site however you want !

## Global Theme Switch 

Without a SPA framwork, it may seem hard to do stuff like global page themeing, but with soupault and awsm, this is as easy as setting some 
variables in the browsers session storage and embedding it wherever I want it. This is one example of a static UI component we want to 
widgetize with Soupault.  

# Pandoc

## The Basic Command

## Documents FTW


## The Config File

## Updating The Site

# Javascript

## No JS Needed (progressive enhancement)

## Adding JS

# Ugly Build Files

If your using a bunch of custom CSS and maybe some JS, your build files may start to get really big and unreadable

## What Do

Ultimately, you can choose to separate all your CSS and JS if you want. You can separate them into 'modules' (separate files)
and inject static tags to link them in the head of your document. This way, your build will have only html files full of mark up.

## Big Files For The Browser

At the end of the day, I prefer to keep my templates encapsulated, despite large file sizes. Since I can simply manipulate my sites files
during the development process, I don't need to edit the built files, which ineveitably will be large and contain lots of individual
styles and scripts in one big inline file.

When using an SSG like this one, I basically consider my code to be compiling to html, which the server will read, out of the entire web
stack as one encapsulated unit. Sort of like Svelte, but as a SSG. Pre-compiler?

In other words, Soupault won't impose anything on you, it's just up to how you want to organize your site. You can use Soupault as little
or as much as you need.

# Static App Future (??)

## Unix-likes as the web dev API, CGI scripting

## Vanilla JS UI, As God Intended

## Turbo

## A Semantic Web Editor Would Be An Incredibly Simple Tool That Would Change The Industry

## Back To Lynx




